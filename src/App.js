import React, { useEffect } from "react";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import io from "socket.io-client";

var socket = io("http://localhost:3001", { transports: ["websocket"] });
function App() {
  useEffect(() => {
    socket.on("getData", (res) => console.log(res));
  }, [socket]);
  return (
    <BrowserRouter>
      <div className="App">
        <Routes Routes>
          <Route path="/" exact element={<HomePage />}></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
