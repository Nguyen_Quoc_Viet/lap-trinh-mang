import React,{useState} from "react";

function Rank() {
    const data=[
        {
            name:'Việt Nam',
            score:'100'
        },
        {
            name:'Hàn',
            score:'50'
        },
        {
            name:'Nhật',
            score:'45'
        },
        {
            name:'Trung',
            score:'30'
        },
        {
            name:'Lào',
            score:'65'
        },
        {
            name:'CamPuchia',
            score:'98'
        },
        {
            name:'Mĩ',
            score:'0'
        },
        {
            name:'Đông ti mo',
            score:'55'
        }
    ]

    const [dataNgoaiHangAnh, setDataNgoaiHangAnh] = useState(data)
    const [dataSeria, setDataSeria] = useState(data)
    const [dataBundesliga, setDataBundesliga] = useState(data)
    const [dataLigue1, setDataLigue1] = useState(data)
    const [dataLaliga, setDataLaliga] = useState(data)
  return (
    <div>
      <section className="hp_banner_area section_padding">
        <div className="container">
          <div className="row">


              {/* //Ngoại hạng anh */}
            <div className="rank_box">
              <div className="rank_box_header">
                <div className="rank_box_logo">
                  <img
                    src="https://s.vnecdn.net/thethao/restruct/i/v67/dulieubongda/icons/nha.svg"
                    alt=""
                  />
                </div>
                <p>Ngoại hạng anh</p>
                <span>Điểm</span>
              </div>
              <div className="rank_box_list">
                {dataNgoaiHangAnh&&dataNgoaiHangAnh.map((item,index)=>(
                    <div key={index} className="rank_box_item">
                    <span className="rank_box_key">{index+1}</span>
                    <span className="rank_box_name">{item.name}</span>
                    <span className="rank_box_score">{item.score}</span>
                  </div>
                ))}
              </div>
            </div>

                            {/* //Laliga */}

            <div className="rank_box">
              <div className="rank_box_header">
                <div className="rank_box_logo">
                  <img
                    src="https://s1.vnecdn.net/vnexpress/restruct/c/v919/dulieubongda/pc/images/graphics/la_liga_lite.svg"
                    alt=""
                  />
                </div>
                <p>La liga</p>
                <span>Điểm</span>
              </div>
              <div className="rank_box_list">
                {dataSeria&&dataSeria.map((item,index)=>(
                    <div key={index} className="rank_box_item">
                    <span className="rank_box_key">{index+1}</span>
                    <span className="rank_box_name">{item.name}</span>
                    <span className="rank_box_score">{item.score}</span>
                  </div>
                ))}
              </div>
            </div>

{/* //seria */}

            <div className="rank_box">
              <div className="rank_box_header">
                <div className="rank_box_logo">
                  <img
                    src="https://s.vnecdn.net/thethao/restruct/i/v67/dulieubongda/icons/seria.svg"
                    alt=""
                  />
                </div>
                <p>Seria</p>
                <span>Điểm</span>
              </div>
              <div className="rank_box_list">
                {dataSeria&&dataSeria.map((item,index)=>(
                    <div key={index} className="rank_box_item">
                    <span className="rank_box_key">{index+1}</span>
                    <span className="rank_box_name">{item.name}</span>
                    <span className="rank_box_score">{item.score}</span>
                  </div>
                ))}
              </div>
            </div>


{/* //buldesliga */}
            <div className="rank_box">
              <div className="rank_box_header">
                <div className="rank_box_logo">
                  <img
                    src="https://s.vnecdn.net/thethao/restruct/i/v67/dulieubongda/icons/buldesliga.svg"
                    alt=""
                  />
                </div>
                <p>Buldesliga</p>
                <span>Điểm</span>
              </div>
              <div className="rank_box_list">
                {dataBundesliga&&dataBundesliga.map((item,index)=>(
                    <div key={index} className="rank_box_item">
                    <span className="rank_box_key">{index+1}</span>
                    <span className="rank_box_name">{item.name}</span>
                    <span className="rank_box_score">{item.score}</span>
                  </div>
                ))}
              </div>
            </div>


{/* //ligue1 */}
            <div className="rank_box">
              <div className="rank_box_header">
                <div className="rank_box_logo">
                  <img
                    src="https://s.vnecdn.net/thethao/restruct/i/v67/dulieubongda/icons/ligue1.svg"
                    alt=""
                  />
                </div>
                <p>Ligue 1</p>
                <span>Điểm</span>
              </div>
              <div className="rank_box_list">
                {dataLigue1&&dataLigue1.map((item,index)=>(
                    <div key={index} className="rank_box_item">
                    <span className="rank_box_key">{index+1}</span>
                    <span className="rank_box_name">{item.name}</span>
                    <span className="rank_box_score">{item.score}</span>
                  </div>
                ))}
              </div>
            </div>



            


          </div>
        </div>
      </section>
    </div>
  );
}

export default Rank;
