import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Spinner } from "reactstrap";

import "bootstrap/dist/css/bootstrap.min.css";
import io from "socket.io-client";
const socket = io("api.itcode.vn:3005", { transports: ["websocket"] });
function MainSection1() {
  const [data, setData] = useState(null);
  const [dataRight, setDataRight] = useState([]);
  // lấy tin tức
  useEffect(() => {
    socket.on("getData", (res) => {
      setData(res[0]);
      setDataRight([res[1], res[2], res[3]]);
    });
  }, [socket]);
  return (
    <div>
      <section className="hp_banner_area section_padding">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="hp_banner_box">
                {data ? (
                  <div className="hp_banner_left">
                    <div className="section_left_box">
                      <div className="section_img_left">
                        <img src={data?.image} alt="img" />
                      </div>
                      <div className="section_post_left">
                        <a style={{textDecoration: 'none'}} href={data?.linkArticle} className="section_post_title">
                          {data?.title}
                        </a>
                        {/* <div className="section_post_title"></div> */}
                        <div className="section_post_content">{data?.description}</div>
                      </div>
                    </div>
                  </div>
                ) : (
                  <Spinner />
                )}
                <div className="hp_banner_right">
                  {dataRight &&
                    dataRight.map((item, index) => (
                      <div key={index} className="father_box">
                        <div className="box_box_box">
                          <a style={{textDecoration: 'none'}} href={item?.linkArticle} className="title_section1">
                            {item?.title}
                          </a>
                          <div className="des_section1">{item.description}</div>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default MainSection1;
