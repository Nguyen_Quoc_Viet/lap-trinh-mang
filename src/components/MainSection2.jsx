import React from "react";
import FashionAreal from './FashionArea'
import HomeSideBar from "./HomeSideBar";
function MainSection2() {
  
  return (
    <div>
      <section className="main_news_wrapper">
        <div className="container">
          <div className="row">
            <div className="col-md-9 col-sm-9 col-xs-12">
              {/* ~~~=| Fashion area START |=~~~ */}
              <FashionAreal></FashionAreal>
              {/* ~~~=| Fashion area END |=~~~ */}
            </div>
            <div className="col-md-3 col-sm-3 col-xs-12">
              <HomeSideBar></HomeSideBar>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default MainSection2;
