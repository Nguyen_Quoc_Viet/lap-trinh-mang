import React, { useState } from 'react'
import {Link } from 'react-router-dom'
import side1 from '../images/side1.jpg'
import side2 from '../images/side2.jpg'
import side3 from '../images/side3.jpg'
import side4 from '../images/side4.jpg'
import side5 from '../images/side5.jpg'
function LastedNews() {
    const fakeData=[
        {
            img:side1,
            title:'Suy nghĩ về tình cờ vẫn sang trọng',
            date:"1 giờ trước"
        },
        {
            img:side2,
            title:'Suy nghĩ về tình cờ vẫn sang trọng',
            date:"1 giờ trước"
        },
        {
            img:side3,
            title:'Suy nghĩ về tình cờ vẫn sang trọng',
            date:"1 giờ trước"
        },
        {
            img:side4,
            title:'Suy nghĩ về tình cờ vẫn sang trọng',
            date:"1 giờ trước"
        },
        {
            img:side5,
            title:'Suy nghĩ về tình cờ vẫn sang trọng',
            date:"1 giờ trước"
        },

    ]
    const [lastedNews, setLastedNews] = useState(fakeData)
    // useEffect(() => {
  //   const getLatedsNews=async()=>{
  //     const res=axios.get(``);
  //     setLastedNews(res.data)
      
  //   }
  //   geLastedNews()
  // }, [])
    return (
        <div>
            {lastedNews&&lastedNews.map((item,index)=>(
                <div key={index} className="fs_news_right">
                <div className="single_fs_news_img">
                  {" "}
                  <img alt="Single News" src={item.img} />{" "}
                </div>
                <div className="single_fs_news_right_text">
                  <h4>
                    <Link to="blog-single-slider-post.html">
                      {item.title}
                    </Link>
                  </h4>
                  <p>
                    {" "}
                    <Link to="/">Fashion | </Link>{" "}
                    <i className="fa fa-clock-o" /> {item.date}{" "}
                  </p>
                </div>
              </div>
            ))}
        </div>
    )
}

export default LastedNews
