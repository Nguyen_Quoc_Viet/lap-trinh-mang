import React,{useState} from 'react'
import {Link} from 'react-router-dom'
import htAdd from "../images/ht-add.jpg";
import vid1 from "../images/vid1.jpg";
import vid2 from "../images/vid2.jpg";
import vid3 from "../images/vid3.jpg";
import vid4 from "../images/vid4.jpg";
import hor_dot from '../images/hor_dot.png'
function Video() {
    const fakeData = [
        {
          img: vid2,
          title: "Suy nghĩ về tình cờ vẫn sang trọng",
    
          date: "1 giờ trước",
        },
        {
          img: vid3,
          title: "Hipster Yoga ở ngày tận thế",
    
          date: "1 ngày trước",
        },
        {
          img: vid4,
          title: "Nền độc lập của Scotland mà lịch sử cho chúng ta biết",
          date: "15 phút trước",
        },
      ];
      const [Video, setVideo] = useState(
        fakeData
      );
    return (
        <div>
            <div className="home_add_box">
              {" "}
              <img src={htAdd} alt="add" />{" "}
            </div>
            <div className="header_fasion gadgets_heading">
              <div className="left_fashion main_nav_box">
                <ul>
                  <li className="nav_Video_post">
                    <Link to="/">Video</Link>
                  </li>
                </ul>
              </div>
              <div className="fasion_right">
                {" "}
                <Link to="/">
                  <img src={hor_dot} alt="/" />
                </Link>{" "}
              </div>
            </div>
            <div className="fashion_area_box Video_area_box">
              <div className="row">
                <div className="col-md-6 col-sm-6 col-xs-12">
                  <div className="all_news_right">
                    {Video &&
                      Video.map((item, index) => (
                        <div key={index} className="fs_news_right">
                          <div className="single_fs_news_img">
                            {" "}
                            <img src={item.img} alt="Single News" />
                            <div className="br_cam">
                              {" "}
                              <Link className="fa fa-camera" to="/" />{" "}
                            </div>
                          </div>
                          <div className="single_fs_news_right_text">
                            <h4>
                              <Link to="blog-single-slider-post.html">
                                {item.title}
                              </Link>
                            </h4>
                            <p>
                              {" "}
                              <Link className="Video_f" to="/">
                                Fashion{" "}
                              </Link>
                              | <i className="fa fa-clock-o" />
                              {item.date}{" "}
                            </p>
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
                <div className="col-md-6 col-sm-6 col-xs-12">
                  <div className="fs_news_left fs_news_vid_right">
                    <div className="single_fs_news_left_text">
                      <div className="fs_news_left_img">
                        {" "}
                        <img src={vid1} alt="Video" />
                        <div className="br_cam br_vid_big">
                          {" "}
                          <Link className="fa fa-caret-right" to="/" />{" "}
                        </div>
                      </div>
                      <h4>
                        <Link to="blog-single-slider-post.html">
                          Cổ phiếu đóng cửa trên $ 100, đánh dấu một đợt bán
                        </Link>
                      </h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    )
}

export default Video
