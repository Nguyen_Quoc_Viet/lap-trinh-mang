import React,{useState} from 'react'
import {Link} from 'react-router-dom'
import fasn1 from "../images/fasn1.png";
import fasn2 from "../images/fasn2.png";
import fasn3 from "../images/fasn3.png";
import ph from "../images/ph.jpg";
function FashionHeader() {
    const fakeData = [
        {
          img: fasn1,
          title: "Suy nghĩ về tình cờ vẫn sang trọng",
          category: "Fashion",
          date: "1 giờ trước",
        },
        {
          img: fasn2,
          title: "Họ đang mặc đẹp nhất từ tuần lễ Paris",
          category: "Fashion",
          date: "1 ngày trước",
        },
        {
          img: fasn3,
          title: "Xu hướng bơi lội của Cruise 2015: Đường mờ",
          category: "Fashion",
          date: "15 phút trước",
        },
      ];
      const [fashionHeader,setFashionHeader] = useState(fakeData);
    return (
        <div>
            <div className="header_fasion">
              <div className="left_fashion main_nav_box">
                <ul>
                  <li className="nav_fashion">
                    <Link to="/">Thời trang</Link>
                  </li>
                </ul>
              </div>
              <div className="fasion_right">
                <ul>
                  <li>
                    <Link to="/">Phong cách</Link>
                  </li>
                  <li>
                    <Link to="/">Sống thông minh</Link>
                  </li>
                  <li>
                    <Link to="/">Thời trang theo tuần</Link>
                  </li>
                  <li className="last_item">
                    <Link to="/">...</Link>
                  </li>
                </ul>
              </div>
            </div>
            <div className="fashion_area_box">
              <div className="row">
                <div className="col-md-6 col-sm-6 col-xs-12">
                  <div className="fs_news_left">
                    <div className="single_fs_news_left_text">
                      <div className="fs_news_left_img">
                        {" "}
                        <img src={ph} alt="/" />
                        <div className="br_cam br_vid_big_s">
                          {" "}
                          <Link className="fa fa-camera" to="/" />{" "}
                        </div>
                      </div>
                      <h4>
                        <Link to="blog-single-slider-post.html">
                          Đi Du Lịch Với Công Nghệ Một Số Mẹo Từ.
                        </Link>
                      </h4>
                      <p>
                        {" "}
                        <i className="fa fa-clock-o" /> 3 phút trước{" "}
                        <i className="fa fa-comment" /> 19{" "}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-6 col-sm-6 col-xs-12">
                  <div className="all_news_right">
                    {fashionHeader &&
                      fashionHeader.map((item, index) => (
                        <div key={index} className="fs_news_right">
                          <div className="single_fs_news_img">
                            {" "}
                            <img src={item.img} alt="Single News" />{" "}
                          </div>
                          <div className="single_fs_news_right_text">
                            <h4>
                              <Link to="blog-single-slider-post.html">
                                {item.title}
                              </Link>
                            </h4>
                            <p>
                              {" "}
                              <Link to="/">{item.category} </Link>|{" "}
                              <i className="fa fa-clock-o" /> {item.date}{" "}
                            </p>
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            </div>
        </div>
    )
}

export default FashionHeader
