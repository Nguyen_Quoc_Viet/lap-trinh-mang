import React from 'react'
import {Link} from 'react-router-dom'
import hor_dot from '../images/hor_dot.png'
import GadgetLeft from './GadgetLeft'
import GadgetRight from './GadgetRight'
function Gadget() {
    
    return (
        <div>
            <div className="header_fasion gadgets_heading">
                      <div className="left_fashion main_nav_box">
                        <ul>
                          <li className="nav_gadgets">
                            <Link to="/">DỤNG CỤ</Link>
                          </li>
                        </ul>
                      </div>
                      <div className="fasion_right">
                        {" "}
                        <Link to="/">
                          <img src={hor_dot} alt="/" />
                        </Link>{" "}
                      </div>
                    </div>
                    <div className="gadgets_area_box">
                      <div className="row">
                        <div className="col-md-6 col-sm-6 col-xs-12">
                          <GadgetRight></GadgetRight>
                        </div>
                        <div className="col-md-6 col-sm-6 col-xs-12">
                          <GadgetLeft></GadgetLeft>
                        </div>
                      </div>
                    </div>
        </div>
    )
}

export default Gadget
