import React, { useState } from "react";
import Iframe from "react-iframe";
import { Link } from "react-router-dom";
import footer_logo from "../images/footer_logo.png";
import s_rl_pic1 from "../images/s_rl_pic1.jpg";
import s_rl_pic3 from "../images/s_rl_pic3.jpg";
import s_rl_pic2 from "../images/s_rl_pic2.jpg";
// import axios from 'axios'

function Footer() {
  const fakeData = [
    {
      img: s_rl_pic1,
      title: "Chúng tôi có thể thiết kế cho mong muốn của bạn. Chúng tôi ở đây ...",
      date: "06 12 2000",
    },
    {
      img: s_rl_pic2,
      title: "Chúng tôi có thể thiết kế cho mong muốn của bạn. Chúng tôi ở đây ...",
      date: "06 12 2001",
    },
    {
      img: s_rl_pic3,
      title: "Chúng tôi có thể thiết kế cho mong muốn của bạn. Chúng tôi ở đây ...",
      date: "06 12 2002",
    },
  ];
  const [news, setNews] = useState(fakeData);
  // lấy tin tức mới nhất
  // useEffect(() => {
  //   const getNews = async () => {
  //     const res = await axios.get(``);
  //     setNews(res.data)
  //   };
  //   getNews();
  // }, [])
  return (
    <div>
      <footer className="footer_area">
        <div className="footer_top">
          <div className="container">
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">
                <div className="footer_top_box">
                  <div className="row">
                    <div className="col-md-4 col-sm-4 col-xs-12">
                      <div className="footer_top_left">
                        {" "}
                        <img src={footer_logo} alt="Footer Logo" />
                        <p>Chúng tôi đã mang đến hàng triệu cung bậc cảm xúc khác nhau cho người đọc. Hãy đếm với chúng tôi mỗi ngày để cập nhật nhiều tin tức mới nhất</p>
                        <div className="ft_contact">
                          <h4>Liên hệ</h4>
                          <address>
                            <p>
                              {" "}
                              <i className="fa fa-map-marker" />
                              Thảo điền, quận 2, thành phố Hồ Chí Minh
                            </p>
                            <Link to="tel:+100012354657">
                              <i className="fa fa-phone" />
                              Điện thoại : +84 891691852
                            </Link>
                            <br />
                            <Link to="mail-to:Info@News.com">
                              <i className="fa fa-phone" /> E-mail : thongtin@gmail.com
                            </Link>
                          </address>
                        </div>
                        <div className="ft_connected">
                          <h4>Kết nối </h4>
                          <p>Theo dõi chúng tôi trên những nền tảng mạng xã hội đa dạng</p>
                          <ul>
                            <li>
                              <Link className="fa fa-facebook" to="/" />
                            </li>
                            <li>
                              <Link className="fa fa-twitter" to="/" />
                            </li>
                            <li>
                              <Link className="fa fa-google-plus" to="/" />
                            </li>
                            <li>
                              <Link className="fa fa-linkedin" to="/" />
                            </li>
                            <li>
                              <Link className="fa fa-tumblr" to="/" />
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-4 col-xs-12">
                      <div className="footer_top_middle">
                        <div className="ftm_realted_post">
                          <h4>Bài đăng phổ biến</h4>
                          {news &&
                            news.map((item, index) => (
                              <div key={index} className="single_related_post">
                                <div className="srp_img">
                                  {" "}
                                  <Link to="blog-single-slider-post.html">
                                    {" "}
                                    <img src={item.img} alt="/" />
                                  </Link>{" "}
                                </div>
                                <div className="rel_post_text">
                                  <span>{item.date}</span>
                                  <p>{item.title}</p>
                                </div>
                              </div>
                            ))}
                        </div>
                        <div className="ftm_newsletter">
                          <h4>Bảng tin </h4>
                          <p>Đăng kí để nghe và nhận thông tin mới nhất hằng ngày qua thư điện tử từ chúng tôi</p>
                          <form>
                            <input
                              style={{
                                outline: "none",
                              }}
                              type="text"
                              placeholder="Nhập địa chỉ email"
                              defaultValue=""
                            />
                            <button type="submit" className="fm_newsletter" />
                          </form>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-4 col-xs-12">
                      <div className="footer_top_right">
                        <div className="ftr_video">
                          <h4>video trong ngày</h4>
                          <div className="results_video embed-responsive embed-responsive-16by9">
                            <Iframe className="embed-responsive-item" width={560} height={350} src="https://www.youtube.com/embed/ScMzIvxBSi4" allowFullScreen />
                          </div>
                        </div>
                        <div className="ftr_tags">
                          <h4>Tags</h4>
                          <ul id="tag">
                            <li>
                              <Link to="/">Android</Link>
                            </li>
                            <li>
                              <Link to="/">Apple</Link>
                            </li>
                            <li>
                              <Link to="/">Trò chơi</Link>
                            </li>
                            <li>
                              <Link to="/">Phần mềm và ứng dụng</Link>
                            </li>
                            <li>
                              <Link to="/">Thiêt kế </Link>
                            </li>
                            <li>
                              <Link to="/">Bộ sưu tập</Link>
                            </li>
                            <li>
                              <Link to="/">Cuộc sống và phong cách</Link>
                            </li>
                            <li>
                              <Link to="/">Công nghệ</Link>
                            </li>
                            <li>
                              <Link to="/">Thể thao</Link>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer_bottom">
          <div className="container">
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">
                <div className="footer_bottom_box">
                  <p> ©2021 </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
