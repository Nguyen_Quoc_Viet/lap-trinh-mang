import React from "react";
import { Link } from "react-router-dom";
import sidebar1 from "../images/sidebar1.jpg";
import side_slider from "../images/side_slider.jpg";
import FollowUs from "./FollowUs";
import LastedNews from "./LastedNews";
function HomeSideBar() {
  return (
    <div>
      <div className="home_sidebar">
        <div className="purchase_sidebar">
          <div className="purchase_sidebar_img">
            {" "}
            <img src={sidebar1} alt="/" />
            <div className="purchase_overlay" />
            <div className="purchase_sidebar_text">
              <h2>Nhanh</h2>
              <p>
                Cung cấp cho trang web của bạn một thiết kế lại nó xứng đáng
              </p>
              <div className="purchase_s">
                {" "}
                <Link to="/">Mua</Link>{" "}
              </div>
            </div>
          </div>
        </div>
        <FollowUs></FollowUs>
        <div className="follow_us_side">
          <h2>BÀI ĐĂNG MỚI NHẤT</h2>
          <div className="all_news_right">
            <LastedNews></LastedNews>
          </div>
        </div>
        <div className="follow_us_side">
          <h2>thư viện hình ảnh</h2>
          <div
            id="carousel-example-generic"
            className="carousel slide"
            data-ride="carousel"
          >
            {/* Indicators */}
            <ol className="carousel-indicators">
              <li
                data-target="#carousel-example-generic"
                data-slide-to={0}
                className="active"
              />
              <li data-target="#carousel-example-generic" data-slide-to={1} />
              <li data-target="#carousel-example-generic" data-slide-to={2} />
            </ol>
            {/* Wrapper for slides */}
            <div className="carousel-inner" role="listbox">
              <div className="item active">
                <div className="br_single_news">
                  {" "}
                  <img alt="/" img={side_slider} />
                  <div className="br_cam">
                    {" "}
                    <Link className="fa fa-camera" to="/" />{" "}
                  </div>
                </div>
              </div>
              <div className="item">
                <div className="br_single_news">
                  {" "}
                  <img alt="/" src={side_slider} />
                  <div className="br_cam">
                    {" "}
                    <Link className="fa fa-camera" to="/" />{" "}
                  </div>
                </div>
              </div>
              <div className="item">
                <div className="br_single_news">
                  {" "}
                  <img alt="/" src={side_slider} />
                  <div className="br_cam">
                    {" "}
                    <Link className="fa fa-camera" to="/" />{" "}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HomeSideBar;
