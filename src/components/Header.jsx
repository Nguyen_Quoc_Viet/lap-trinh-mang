import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import logo from "../images/logo.png";
// import axios from 'axios'
import header_add from "../images/header-add.jpg";
import megapic00 from "../images/megapic00.jpg";
import megapic01 from "../images/megapic01.jpg";
import megapic02 from "../images/megapic2.jpg";
import megapic03 from "../images/megapic03.jpg";

function Header() {
  const fakeData = [
    {
      img: megapic01,
      title: "haha haha",
      date: "01 02 2002",
    },
    {
      img: megapic02,
      title: "haha hihi",
      date: "01 02 2003",
    },
    {
      img: megapic03,
      title: "haha haha",
      date: "01 02 2004",
    },
  ];
  const [newsSoccer, setNewsSoccer] = useState(fakeData);
  // useEffect(() => {
  //   const getNewsSoccer=async()=>{
  //     const res=axios.get(``);
  //     res.data? setNewsSoccer(res.data):setNewsSoccer(fakeData)

  //   }
  //   getNewsSoccer()
  // }, [])
  return (
    <div>
      <header className="header_area">
        <div className="header_top">
          <div className="container">
            <div className="row">
              <div className="col-md-6 col-sm-6 col-xs-12">
                <div className="header_top_left">
                  <div className="news_twiks">
                    <h4>Cung cấp tin tức</h4>
                  </div>
                  <div className="news_twiks_items">
                    <div id="carousel-example-generic1" className="carousel slide" data-ride="carousel">
                      {/* Wrapper for slides */}
                      <div className="carousel-inner" role="listbox">
                        <div className="item active">
                          <p>Nhanh nhất</p>
                        </div>
                        <div className="item">
                          <p>Chính xác nhất</p>
                        </div>
                        <div className="item">
                          <p>Nóng nhất</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-6 col-xs-12">
                <div className="header_top_right">
                  <div className="social_header">
                    <ul>
                      <li>
                        <Link className="fa fa-facebook" to="/" />
                      </li>
                      <li>
                        <Link className="fa fa-twitter" to="/" />
                      </li>
                      <li>
                        <Link className="fa fa-google-plus" to="/" />
                      </li>
                      <li>
                        <Link className="fa fa-linkedin" to="/" />
                      </li>
                      <li>
                        <Link className="fa fa-behance" to="/" />
                      </li>
                    </ul>
                  </div>
                  <div className="header_search_box">
                    <form className="header_search hidden-xs" action="index.html">
                      <input type="text" placeholder="Search" />
                      <input type="submit" defaultValue />
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* ~~~=| Logo Area START |=~~~ */}
        <div className="header_logo_area">
          <div className="container">
            <div className="row">
              <div className="col-md-4 col-sm-4 col-xs-12">
                <div className="logo">
                  {" "}
                  <Link to="index.html">
                    <img src={logo} alt="Site Logo" />
                  </Link>{" "}
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* ~~~=| Logo Area END |=~~~ */}
        {/* ~~~=| Main Navigation START |=~~~ */}
        <div className="mainnav">
          <div className="container">
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">
                <nav className="main_nav_box">
                  <ul id="nav">
                    <li className="nav_lifeguide">
                      <Link to="/">Trang </Link>
                      <div className="sub_menu sub_menu_p single_mega">
                        <ul>
                          <li>
                            <Link to="contact.html">Blog Detail Pages</Link>
                            <ul className="lev2sub lev2subp">
                              <li>
                                <Link to="blog-single-audio-post.html">Trang nghe audio</Link>
                              </li>
                              <li>
                                <Link to="blog-single-link-post.html">Trang liên kết bóng đá</Link>
                              </li>
                              <li>
                                <Link to="blog-single-post.html">Trang bài đăng</Link>
                              </li>
                              <li>
                                <Link to="blog-single-quote-post.html">Trang đánh giá</Link>
                              </li>
                              <li>
                                <Link to="blog-single-slider-post.html">Trang slider</Link>
                              </li>
                              <li>
                                <Link to="blog-single-twitter-post.html">Trang Twitter</Link>
                              </li>
                              <li className="last-child">
                                <Link to="blog-single-video-post.html">Trang Video</Link>
                              </li>
                            </ul>
                          </li>
                          <li>
                            <Link to="about.html">Về chúng tôi</Link>
                          </li>
                          <li>
                            <Link to="contact.html">Kết nối</Link>
                          </li>
                        </ul>
                      </div>
                    </li>
                    <li className="nav_fashion">
                      <Link to="/">Thời trang</Link>
                    </li>
                    <li className="nav_gadgets">
                      <Link to="/">Dụng cụ</Link>
                    </li>
                    <li className="nav_lifestyle">
                      <Link to="/">Phong cách sống</Link>
                    </li>
                    <li className="nav_video">
                      <Link to="/">Video</Link>
                    </li>
                    <li className="nav_features">
                      <Link to="/">Tương lai</Link>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
          {/* ~~~=| Main Navigation END |=~~~ */}
        </div>
      </header>
    </div>
  );
}

export default Header;
