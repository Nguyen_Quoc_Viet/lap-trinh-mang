import React,{useState} from 'react'
import {Link} from 'react-router-dom'
import grd1 from '../images/grd1.jpg'
import gred2 from '../images/gred2.jpg'
import gres3 from '../images/gres3.jpg'
import gradient1 from '../images/gradient1.jpg'

function GadgetRight() {

    const fakeData=[
        {
          img: grd1,
          title:'Những kẻ troll trên Internet rất thích làm bạn đau khổ',
          
          date:'1 giờ trước'
        },
        {
          img: gred2,
          title:'Tại sao máy gia tốc công nghệ có thể sớm không liên quan như một MBA',
          
          date:'1 ngày trước'
        },
        {
          img: gres3,
          title:'Tại sao máy gia tốc công nghệ có thể sớm không liên quan như một MBA',
          date:'15 phút trước'
      }
      ]

      const [gadgetRight, setGadgetRight] = useState(fakeData)

     // useEffect(() => {
  //   const getGadgetLeft=async()=>{
  //     const res=axios.get(``);
  //     setGadgetLeft(res.data)
      
  //   }
  //   getGadgetLeft()
  // }, [])
    return (
        <div>
            <div className="fs_news_left fs_gadgets_news_left">
                            <div className="fs_news_left_img_g">
                              {" "}
                              <img src={gradient1} alt="/" />
                              <div className="br_cam br_vid_big">
                                {" "}
                                <Link
                                  className="fa fa-caret-right"
                                  to="/"
                                />{" "}
                              </div>
                            </div>
                            <div className="single_fs_news_left_text">
                              <h4>
                                <Link to="blog-single-slider-post.html">
                                Mẹo để chụp những bức ảnh thành phố tuyệt vời với
                                   điện thoại thông minh
                                </Link>
                              </h4>
                              <p>
                                {" "}
                                <i className="fa fa-clock-o" /> 3 min ago{" "}
                                <i className="fa fa-comment" /> 19{" "}
                              </p>
                            </div>
                            <div className="all_news_right">
                              {gadgetRight&&gadgetRight.map((item,index)=>(
                                  <div key={index} className="fs_news_right">
                                  <div className="single_fs_news_img">
                                    {" "}
                                    <img
                                      src={item.img}
                                      alt="Single News"
                                    />{" "}
                                  </div>
                                  <div className="single_fs_news_right_text">
                                    <h4>
                                      <Link to="blog-single-slider-post.html">
                                        {item.title}
                                      </Link>
                                    </h4>
                                    <p>
                                      {" "}
                                      <Link className="gad_color" to="/">
                                        Dụng cụ
                                      </Link>
                                      | <i className="fa fa-clock-o" /> {item.date}
                                    </p>
                                  </div>
                                </div>
                              ))}
                              
                            </div>
                          </div>
        </div>
    )
}

export default GadgetRight
