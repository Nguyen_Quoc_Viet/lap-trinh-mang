import React, { useState } from "react";
import { Link } from "react-router-dom";
import lis2 from "../images/lis2.jpg";
import lis3 from "../images/lis3.jpg";
import lis4 from "../images/lis4.jpg";
import lifes1 from "../images/lifes1.jpg";
import hor_dot from "../images/hor_dot.png";
function LifeStyle() {
  //tin tức lối sống
  const fakeData = [
    {
      img: lis2,
      title: "Suy nghĩ về tình cờ vẫn sang trọng",

      date: "1 giờ trước",
    },
    {
      img: lis3,
      title: "Hipster Yoga ở ngày tận thế",

      date: "1 ngày trước",
    },
    {
      img: lis4,
      title: "Xu hướng bơi lội của Cruise 2015: Đường mờ",

      date: "15 phút trước",
    },
  ];
  const [lifeStyle, setLifeStyle] = useState(fakeData);
  // lấy tin tức lối sống
  // useEffect(() => {
  //   const getLifeStyle = async () => {
  //     const res = await axios.get(``);
  //     setLifeStyle(res.data)
  //   };
  //   getLifeStyle();
  // }, [])
  return (
    <div>
      <div className="header_fasion gadgets_heading lifestyle_post_area">
        <div className="left_fashion main_nav_box">
          <ul>
            <li className="nav_lifestyle">
              <Link to="/">Lối sống</Link>
            </li>
          </ul>
        </div>
        <div className="fasion_right">
          {" "}
          <Link to="/">
            <img src={hor_dot} alt="/" />
          </Link>{" "}
        </div>
      </div>
      <div className="fashion_area_box">
        <div className="row">
          <div className="col-md-6 col-sm-6 col-xs-12">
            <div className="fs_news_left">
              <div className="single_fs_news_left_text">
                <div className="fs_news_left_img">
                  {" "}
                  <img src={lifes1} alt="life" />
                  <div className="br_cam">
                    {" "}
                    <Link className="fa fa-camera" to="/" />{" "}
                  </div>
                </div>
                <h4>
                  <Link to="blog-single-slider-post.html">Đi du lich với một số mẹo từ</Link>
                </h4>
                <p>
                  {" "}
                  <i className="fa fa-clock-o" /> 3 phút trước <i className="fa fa-comment" /> 19{" "}
                </p>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-6 col-xs-12">
            <div className="all_news_right">
              {lifeStyle &&
                lifeStyle.map((item, index) => (
                  <div key={index} className="fs_news_right">
                    <div className="single_fs_news_img">
                      {" "}
                      <img src={item.img} alt="Single News" />
                      <div className="br_cam">
                        {" "}
                        <Link className="fa fa-camera" to="/" />{" "}
                      </div>
                    </div>
                    <div className="single_fs_news_right_text">
                      <h4>
                        <Link to="blog-single-slider-post.html">{item.title}</Link>
                      </h4>
                      <p>
                        {" "}
                        <Link to="/">Fashion </Link>| <i className="fa fa-clock-o" /> {item.date}{" "}
                      </p>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LifeStyle;
