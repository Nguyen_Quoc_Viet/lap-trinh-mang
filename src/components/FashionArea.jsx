import React from "react";
import { Link } from "react-router-dom";

import Gadget from "./Gadget";
import LifeStyle from "./LifeStyle";
import FashionHeader from "./FashionHeader";
import Video from "./Video";
function FashionArea() {
  

  

  return (
    <div>
      <div className="fashion_area">
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">
            <FashionHeader></FashionHeader>
            <Gadget></Gadget>
            <LifeStyle></LifeStyle>
            <Video></Video>
            <div className="news_pagination">
              <ul className="news_pagi">
                <li>
                  <Link to="/">2</Link>
                </li>
                <li>
                  <Link to="/">2</Link>
                </li>
                <li>
                  <Link to="/">3</Link>
                </li>
                <li className="dotlia">
                  <Link to="/">. . .</Link>
                </li>
                <li className="nextlia">
                  <Link to="/">Tiếp</Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FashionArea;
