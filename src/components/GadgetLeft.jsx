import React, { useState } from "react";
import { Link } from "react-router-dom";
import grd2 from "../images/grd2.jpg";
import grd3 from "../images/grd3.jpg";
import gred1 from "../images/gred1.jpg";
import gradient2 from "../images/gradient2.jpg";
function GadgetLeft() {
  const fakeData = [
    {
      img: gred1,
      title: "Những kẻ troll trên Internet rất thích làm bạn đau khổ",

      date: "1 giờ trước",
    },
    {
      img: grd2,
      title:
        "Tại sao máy gia tốc công nghệ có thể sớm không liên quan như một MBA",

      date: "1 ngày trước",
    },
    {
      img: grd3,
      title:
        "Tại sao máy gia tốc công nghệ có thể sớm không liên quan như một MBA",
      date: "15 phút trước",
    },
  ];
  const [gadgetLeft, setGadgetLeft] = useState(fakeData);
  // useEffect(() => {
  //   const getGadgetLeft=async()=>{
  //     const res=axios.get(``);
  //     setGadgetLeft(res.data)

  //   }
  //   getGadgetLeft()
  // }, [])
  return (
    <div>
      <div className="fs_news_left fs_gadgets_news_left">
        <div className="fs_news_left_img_g">
          {" "}
          <img src={gradient2} alt="/" />{" "}
        </div>
        <div className="single_fs_news_left_text">
          <h4>
            <Link to="blog-single-slider-post.html">
              Đi Du Lịch Với Công Nghệ Một Số Mẹo Từ.{" "}
            </Link>
          </h4>
          <p>
            {" "}
            <i className="fa fa-clock-o" />
            3 phút trước
            <i className="fa fa-comment" /> 19{" "}
          </p>
        </div>
        <div className="all_news_right">
          {gadgetLeft &&
            gadgetLeft.map((item, index) => (
              <div key={index} className="fs_news_right">
                <div className="single_fs_news_img">
                  {" "}
                  <img src={item.img} alt="Single News" />{" "}
                </div>
                <div className="single_fs_news_right_text">
                  <h4>
                    <Link to="blog-single-slider-post.html">{item.title}</Link>
                  </h4>
                  <p>
                    {" "}
                    <Link className="gad_color" to="/">
                      Gadget{" "}
                    </Link>
                    | <i className="fa fa-clock-o" /> {item.date}
                  </p>
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
}

export default GadgetLeft;
