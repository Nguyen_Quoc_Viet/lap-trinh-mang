import React from 'react'
import {Link} from 'react-router-dom'
function FollowUs() {
    return (
        <div>
            <div className="follow_us_side">
                  <h2>Theo dõi chúng tôi</h2>
                  <div className="all_single_follow">
                    <div className="single_follow_us">
                      {" "}
                      <Link to="/">
                        {" "}
                        <i className="fa fa-facebook" />
                        <br />
                        5638
                        <br />
                        Theo dõi{" "}
                      </Link>{" "}
                    </div>
                    <div className="single_follow_us twit">
                      {" "}
                      <Link to="/">
                        {" "}
                        <i className="fa fa-twitter" />
                        <br />
                        5638
                        <br />
                        Theo dõi{" "}
                      </Link>{" "}
                    </div>
                    <div className="single_follow_us goopl">
                      {" "}
                      <Link to="/">
                        {" "}
                        <i className="fa fa-google-plus" />
                        <br />
                        5638
                        <br />
                        Theo dõi{" "}
                      </Link>{" "}
                    </div>
                    <div className="single_follow_us last_one">
                      {" "}
                      <Link to="/">
                        {" "}
                        <i className="fa fa-dribbble" />
                        <br />
                        5638
                        <br />
                      Theo dõi{" "}
                      </Link>{" "}
                    </div>
                  </div>
                </div>
        </div>
    )
}

export default FollowUs
